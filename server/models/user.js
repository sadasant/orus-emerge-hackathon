var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var exp;
var schema;
var Model;

module.exports = function(_exp) {
    exp = _exp;
    var Model = mongoose.model('User' , schema);
    return Model;
};

schema = new Schema({
    _id:        ObjectId,
    user_id:    { type: String, required: true },
    image_url:  { type: String, required: true },
    alerts:     { type: Number, default: 0 },
    name:       { type: String },
    created_at: { type: Date },
    updated_at: { type: Date }
});


schema.pre('save', function(next) {
    this.updated_at = new Date();
    next();
});
