var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var exp;

module.exports = function(_exp) {
    exp = _exp;

    var Model = mongoose.model('Alert' , schema);

    Model.schema.path('category').validate(function(v) {
        return /road|plumb|police|fire|eco/i.test(v);
    });

    var minute       = 60*1000;
    var remove_every = 30*minute;
    var check_every  = 2*minute;

    setInterval(function() {
        Model.find().where('updated_at').lt(new Date() - remove_every).exec(function(err, data) {
            console.log("REMOVING OLD ALERTS", data.map(function(e) {
                return e.category + e.description;
            }));
            Model.remove({ _id: { $in: data.map(function(a) {
                return a._id;
            }) }}, function(err) {
                if (err) console.error(err);
            });
        });
    }, check_every);

    return Model;
};

var schema = new Schema({
    _id:          ObjectId,
    author_id:    { type: ObjectId, required: true },
    validated_by: [String],
    latitude:     { type: Number, default: 0, required: true },
    longitude:    { type: Number, default: 0, required: true },
    category:     { type: String, required: true },
    description:  { type: String, required: true },
    created_at:   { type: Date },
    updated_at:   { type: Date }
});

schema.pre('save', function(next) {
    this.updated_at = new Date();
    next();
});
