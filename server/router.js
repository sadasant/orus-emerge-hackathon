var secret = require("../secret");

module.exports = function(exp) {
    var controllers = require("./controllers")(exp);

    exp.use(controllers.log);

    exp.get("/", controllers.index);

    // OAUTH
    exp.get("/login", controllers.login);
    exp.get(secret.google.callback_path, controllers.oauth2callback);
    exp.get("/logout", controllers.logout);

    // API
    exp.use("/alerts", controllers.alerts);
    exp.use("/newalert", controllers.newalert);
    exp.get("/validate", controllers.validate);
};
