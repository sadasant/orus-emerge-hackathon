var path         = require("path");
var googleapis   = require('googleapis');
var OAuth2Client = googleapis.OAuth2Client;
var utils        = require('./utils');
var secret       = require("../secret");

var exp;
var db;
var controllers = {};

module.exports = function(_exp) {
    exp = _exp;
    db  = exp.set('db');
    return controllers;
};

controllers.log = function(req, res, next){
    utils.log(req, req.url);
    next();
};

controllers.index = function(req, res, next) {
    // console.log(req.session.plus);
    if (req.session.plus) {
        res.sendfile(path.normalize(__dirname + "/../app.html"));
    } else {
        res.sendfile(path.normalize(__dirname + "/../index.html"));
    }
};

var oas = {};

controllers.login = function(req, res) {
    if (req.session.plus) return res.redirect("/");

    var oa = new OAuth2Client(
        secret.google.client_id,
        secret.google.client_secret,
        secret.google.callback
    );

    var oa_url = oa.generateAuthUrl({
        access_type: 'offline',
        scope: 'https://www.googleapis.com/auth/plus.me'
    });

    res.redirect(oa_url);
};

controllers.oauth2callback = function(req, res) {
    var code = req.query.code;
    if (req.session.plus) return res.redirect("/");
    if (!(code)) return utils.error(500, "oa not found", res);
    utils.log("GOT OAUTH2 CODE", code);

    var plus; // filled in gotPlus

    var oa = new OAuth2Client(
        secret.google.client_id,
        secret.google.client_secret,
        secret.google.callback
    );

    oa.getToken(code, function(err, tokens) {
        if (err) return utils.error(500, "getToken error:" + err.message, res);
        utils.log("GOT OAUTH2 TOKENS", tokens.access_token);
        oa.credentials = {
            access_token: tokens.access_token
        };
        exp.set('oa_client').plus.people
        .get({ userId: 'me' })
        .withAuthClient(oa)
        .execute(gotPlus);
    });

    function gotPlus(err, data) {
        if (err) return utils.error(500, "gotPlus error" + err.message, res);
        plus = data;
        utils.log("GOT PLUS", plus.displayName);
        db.users.findOne({ user_id : plus.id }, foundUser);
    }

    function foundUser(err, user) {
        if (err) return utils.error(500, "foundUser error" + err.message, res);
        if (user) {
            utils.log("GOT USER", user.id, user.name);
            req.session.plus = plus;
            req.session.user = user;
            res.redirect("/");
        } else {
            user = new db.users({
                user_id:   plus.id,
                name:      plus.displayName,
                image_url: plus.image.url
            });
            user.save();
            db.users.findOne({ user_id : plus.id }, foundUser);
        }
    }
};

controllers.logout = function(req, res) {
    req.session.user = undefined;
    req.session.plus = undefined;
    req.session.destroy(function(err) {
        if (err) res.send(500, { error: err });
        res.redirect("/");
    });
};

controllers.alerts = function(req, res) {
    if (!req.session.user) return res.send(500, "NOT AUTHORIZED");
    var latitude  = req.body.latitude;
    var longitude = req.body.longitude;
    var radius    = req.body.radius;
    db.alerts.find()

    // TODO: FIX THIS
    .where('latitude').gt(latitude - radius) // .lt(latitude + radius)
    // .where('latitude').lt(latitude + radius)
    .where('longitude').gt(longitude - radius) // .lte(longitude + radius)

    .limit(10)
    .exec(function(err, alerts) {
        if (err) res.send(500, { error: err });
        var validated = [];
        alerts = alerts.map(function(a) {
            // console.log(req.session.user);
            var _id = req.session.user._id.toString();
            if (a.author_id.toString() === _id || a.validated_by.indexOf(_id) > -1) {
                validated.push(a._id);
            }
            return a;
        });
        res.send({ alerts: alerts, validated: validated });
    });
};

controllers.newalert = function(req, res) {
    if (!req.session.user) return res.send(500, "NOT AUTHORIZED");
    req.body.author_id = req.session.user._id;
    var alert = new db.alerts(req.body);
    alert.save(function(err) {
        if (err) return res.send(500, err);
        res.send("OK");
    });
};

controllers.validate = function(req, res) {
    if (!req.query._id) res.send(500, "Missing _id");
    db.alerts.findOne({ _id: req.query._id }, function(err, a) {
        if (err) return res.send(500, err);
        var _id = req.session.user._id.toString();
        if (a.author_id === req.session.user._id || a.validated_by.indexOf(_id) > -1) {
            return res.send();
        }
        a.validated_by.push(_id);
        db.alerts.update({ _id: req.query._id }, { $set: { validated_by: a.validated_by } }, function(err) {
            if (err) return res.send(500, err);
            res.send();
        });
    });
};
