var port   = process.env.PORT || 8888;
var exp    = require("express")();
var path   = require("path");
var http   = require("http");
var server = http.createServer(exp);
var utils  = require('./utils');

require("./config")(exp);
require("./router")(exp);

server.listen(port, function(){
    utils.log("Server running at http://127.0.0.1:" + port + "/");
});
