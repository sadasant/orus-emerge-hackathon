var utils = module.exports;

utils.parseJSON = function(data) {
    try {
        return JSON.parse(data);
    } catch(e) {
        console.log("JSON.parse body failed!", e);
        return {};
    }
};

utils.log = function() {
    var args = Array.prototype.slice.call(arguments);
    var req = args[0];
    var i   = 1;

    if (typeof req !== "object") {
        req = {};
        i = 0;
    } else {
        args.shift();
    }

    var date  = new Date().toISOString();
    var ip    = req.ip || "";

    if (ip) {
        args = [date, "[" + ip + "]"].concat(args);
    } else {
        args = [date].concat(args);
    }

    console.log.apply(console, args);
};

utils.error = function(code, msg, res) {
    console.error(msg);
    res.send(code, msg);
};
