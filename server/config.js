var path         = require("path");
var express      = require("express");
var cookieParser = require("cookie-parser");
var session      = require("express-session");
var mongoose     = require('mongoose');
var googleapis   = require('googleapis');
var utils        = require('./utils');
var secret       = require("../secret");

module.exports = function(exp) {

    mongoose.connect(secret.mongo_url);
    var db = mongoose.connection;

    db.on("error", function(err) {
        utils.log("DB CONNECTION ERROR", err);
    });

    db.on("open", function(err) {
        utils.log("DB CONNECTED");
    });

    function normalize(dir) {
        return express.static(path.normalize(__dirname + dir));
    }

    exp.use(cookieParser());
    exp.use(session({ secret : secret.cookies_secret, key: "sid"}));

    exp.configure(function(){
        exp.use(express.bodyParser());

        exp.use("/css", normalize("/../css"));
        exp.use("/img", normalize("/../img"));
        exp.use("/js", normalize("/../js"));

        exp.set('db', {
            main:   mongoose,
            users:  require('./models/user')(exp),
            alerts: require('./models/alert')(exp)
        });

        googleapis.discover('plus', 'v1').execute(function(err, client) {
            if (err) return utils.log("Problems during the googleapis client discovery", err);
            exp.set('oa_client', client);
        });
    });
};
