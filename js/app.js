(function() {

    var map;
    var geocoder;
    var service;
    var marker;
    var markers = [];
    var current_latlng;

    var searching    = false;
    var making_alert = false;

    var google = window.google;
    var $      = window.$;

    var $mapContainer    = $("#map-container");
    var $locationName    = $(".location-name");
    var $search          = $("#search");
    var $searchInput     = $("input[name='search']");
    var $blocks          = $(".block");
    var $results         = $("#results");
    var $makeAlert       = $("#make-alert");
    var $makeAlertForm   = $("#make-alert-form");
    var $makeAlertTypes  = $makeAlertForm.find(".types li");
    var $makeAlertAddr   = $makeAlertForm.find("span.address");
    var $makeAlertCancel = $makeAlertForm.find(".cancel");
    var $makeAlertDone   = $makeAlertForm.find(".done");
    var $feed            = $("#feed");

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(gotCurrentPosition, errorCurrentPosition);
    } else {
        // TODO: Replace with a cute alert.
        return errorCurrentPosition("navigator.geolocation is missing");
    }

    function gotCurrentPosition(position) {
        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        map = new google.maps.Map(document.getElementById("map"), {
            center:    latlng,
            zoom:      15,
            // style from: http://snazzymaps.com/style/60/blue-gray
            styles:    [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#b5cbe4"}]},{"featureType":"landscape","stylers":[{"color":"#efefef"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#83a5b0"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#bdcdd3"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e3eed3"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}],
            mapTypeId: 'roadmap'
        });

        resizeMap();

        geocoder = new google.maps.Geocoder();
        service  = new google.maps.places.PlacesService(map);

        updateLocation(latlng);
        google.maps.event.addListener(map, "click", function(event) {
            updateLocation(event.latLng);
        });
    }

    function updateLocation(latLng) {
        current_latlng = latLng;
        geocoder.geocode({ latLng: latLng }, function(results, status) {
            if (status == "OK") {
                var addr = results[0].formatted_address;
                $locationName.text(addr);
                $makeAlertAddr.text(addr);
                if (marker) marker.setMap(null);
                marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: addr
                });
                loadAlerts();
            }
        });
    }

    function errorCurrentPosition(err) {
        console.error(err);
        return window.document.write(JSON.stringify(err));
    }

    function showSearch(results) {
        searching = true;
        $blocks.hide();
        $results.show();
        $results.html("");
        $results.append(results.map(function(e) {
            var $li = $("<li/>");
            $li.text(e.formatted_address);
            $li.data("formatted_address", e.formatted_address);
            $li.data("latitude", e.geometry.location.k);
            $li.data("longitude", e.geometry.location.A);
            return $li;
        }));
    }

    function hideSearch() {
        searching = false;
        $blocks.show();
        $results.hide();
        $results.html();
    }

    $searchInput.on("keyup", function(e) {
        var v = $searchInput.val();
        if (v.length < 3) return hideSearch();
        service.textSearch({
            location: current_latlng,
            radius: '1500',
            query: v
        }, function(results, status) {
            if (status === "OK") {
                showSearch(results);
            }
        });
    });

    $results.on("click", "li", function() {
        var $this  = $(this);
        var latlng = new google.maps.LatLng($this.data("latitude"), $this.data("longitude"));
        map.setCenter(latlng);
        $searchInput.val($this.data("formatted_address"));
        updateLocation(latlng);
        hideSearch();
    });

    function showNewAlertForm() {
        making_alert = true;
        $search.hide();
        $blocks.hide();
        $makeAlertForm.show();
    }

    function hideNewAlertForm() {
        making_alert = false;
        $makeAlertForm.hide();
        $search.show();
        $blocks.show();
        $results.hide();
    }

    $makeAlert.click(function() {
        if (making_alert) return;
        showNewAlertForm();
    });

    $makeAlertCancel.click(hideNewAlertForm);

    $makeAlertTypes.click(function() {
        $makeAlertTypes.removeClass('active');
        $(this).toggleClass('active');
    });

    $makeAlertDone.click(function() {
        var latlng   = current_latlng;
        var category = $makeAlertForm.find(".types li.active").attr("data-value");
        var description = $makeAlertForm.find("textarea").val();
        if (!(category && description)) return;
        $.post("/newalert", {
            latitude:    latlng.k,
            longitude:   latlng.A,
            category:    category,
            description: description
        }, function(status) {
            $makeAlertTypes.removeClass('active');
            $makeAlertForm.find("textarea").val("");
            hideNewAlertForm();
            loadAlerts();
        });
    });

    function loadAlerts() {
        var latlng = current_latlng;
        $.post("/alerts", {
            latitude:  latlng.k,
            longitude: latlng.A,
            radius:    1
        }, function(data) {
            // console.log(data);
            if (data.error) {
                return;
            }
            $feed.html("");
            var _markers = [];
            var alerts = data.alerts;
            alerts.every(function(alert) {
                // console.log(alert);
                var latlng = new google.maps.LatLng(alert.latitude, alert.longitude);
                var msg = alert.category.toUpperCase() + "! " + alert.description;
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: msg
                });
                var infoWindow = new google.maps.InfoWindow({
                    content: msg
                });
                infoWindow.open(map, marker);
                _markers.push(marker);
                var $li = $('<li/>');
                $li.text(msg);
                $li.data("_id", alert._id);
                $li.data("latitude", alert.latitude);
                $li.data("longitude", alert.longitude);
                if (data.validated.indexOf(alert._id) > -1) {
                    $li.data("validated", true);
                    $li.addClass("validated");
                }
                $li.prepend($("<span/>").addClass("validations").text(alert.validated_by.length));
                $feed.append($li);
                return true;
            });
            markers.every(function(e) {
                e.setMap(null);
                return true;
            });
            markers = _markers;
        });
    }

    $feed.on("click", "li", function() {
        var $this = $(this);
        if ($this.hasClass('active') && !$this.data("validated")) {
            $.get("/validate?_id="+$this.data("_id"), function() {
                var $v = $this.find(".validations");
                var v  = $v.text();
                v = parseInt(v, 10);
                $v.text(v+1);
                $this.data("validated", true);
                $this.addClass("validated");
            });
        } else {
            $feed.find("li").removeClass('active');
            $this.toggleClass('active');
            var latlng = new google.maps.LatLng($this.data("latitude"), $this.data("longitude"));
            map.panTo(latlng);
        }
    });

    function resizeMap() {
        var win_width  = $(window).width();
        var noti_width = $("#notifications").width();
        $mapContainer.css({
            left: noti_width+"px",
            width: (win_width - noti_width)+"px"
        });
    }

    $(window).on("resize", resizeMap);

    $("#logout").click(function() {
        window.location.href = "/logout";
    });
 
})();
