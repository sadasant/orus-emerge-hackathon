// SESSIONS
exports.cookies_secret = "kitties";

// MONGOLAB
exports.mongo_url = 'mongodb://user:pass@something.mongolab.com:port/dbname';

// OAUTH
exports.google               = {};
exports.google.client_id     = '';
exports.google.client_secret = '';
exports.google.callback_path = "/oauth2callback";
exports.google.callback      = (process.env.NODE_ENV === 'production' ? 'http://my.custom.url' : 'http://127.0.0.1:' + (process.env.app_port || process.env.PORT || 8888)) + exports.google.callback_path;
